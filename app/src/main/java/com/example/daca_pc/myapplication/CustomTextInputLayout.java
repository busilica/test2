package com.example.daca_pc.myapplication;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

/**
 * Created by daca-pc on 16.05.2017..
 */

public class CustomTextInputLayout extends TextInputLayout{
    public CustomTextInputLayout(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "autarquica.ttf");
        setTypeface(typeface);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "autarquica.ttf");
        setTypeface(typeface);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "autarquica.ttf");
        setTypeface(typeface);
    }
}
