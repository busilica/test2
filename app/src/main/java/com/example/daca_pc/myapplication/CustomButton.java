package com.example.daca_pc.myapplication;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by daca-pc on 16.05.2017..
 */

public class CustomButton extends android.support.v7.widget.AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "autarquica.ttf");
        setTypeface(typeface);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "autarquica.ttf");
        setTypeface(typeface);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "autarquica.ttf");
        setTypeface(typeface);
    }
}
